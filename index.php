<?php
//definition = Aplicar uma variavel de referencia é utilizar um  bloco de função para se
// alterar o valor de uma variavel global


// referencia de variavel passada de fora
// seu valor irá ser alterado de dentro da função
function foo(&$result)
{
    $result = 5; //valor
}
$result = 10;
foo($result);
echo $result; //5

?>

    <table>
      <tr>
        <?php
          for($x=0; $x <= 135; $x++):
            echo "<th> - </th>";
          endfor;
         ?>
      </tr>
    </table>

<?php
function addFive($num){
   $num += 5;
}

function addTwo(&$num){
   $num += 2;
}

$valx = 4;
addFive($valx);
  echo "valor é {$valx}"; //aqui o argumento passado não teve nenhum efeito... 4 Ele apenas foi mostrado

echo "<br>";

addTwo($valx);
  echo "valor é {$valx}"; //aqui seu valor foi somado +2 e foi mostrado

?>
<table>
  <tr>
    <?php
      for($x=0; $x <= 135; $x++):
        echo "<th> - </th>";
      endfor;
     ?>
  </tr>
</table>

<?php
  function test(&$fl)
  {
    $fl++;
  }
  $v = 5;
  test($v);
  echo $v;
 ?>
 <table>
   <tr>
     <?php
       for($x=0; $x <= 135; $x++):
         echo "<th> - </th>";
       endfor;
      ?>
   </tr>
 </table>
