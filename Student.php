<?php

  class Student
  {
    public $value = 10;

    public function &getValue(){
        return $this->value;
    }

  }

  $std = new Student();

  // $v = $std->getValue();
  $value = &$std->getValue(); //possivel alterar o valor da variavel chamando ela na função
  $std->value = 5; //passando referencia
  echo $value;

 ?>
